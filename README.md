
# Welcome!

This is an atlas project skeleton for CDK development with Python.

## Setup

1. Install default packages, add venv, ect with ```pdm install```
2. Replace all occurrences of ```{Name of the app}``` with the actual name  

## Development

Add dependencies only needed for deployment 
```
pmd add -d {PACKAGE_NAME}
```

Add dependencies needed for lambda functions and not provided by aws
```
pmd add -G layer {PACKAGE_NAME}
```

remove dependencies 
```
pmd remove ... {PACKAGE_NAME}
```

## Linting

Format and check your code
```
pdm lint
```

## Deploying

### Local deployment

Make sure that your developer account id is set as an environment variable

```
echo $CDK_DEFAULT_ACCOUNT
```

Otherwise just set it. [How to find your account id](https://docs.aws.amazon.com/IAM/latest/UserGuide/console_account-alias.html#FindingYourAWSId)?
```
export CDK_DEFAULT_ACCOUNT={YOUR_AWS_ACCOUNT_ID}
```

Login to your aws account
```
aws sso login
```

Export requirements and deploy application
```
pdm export-all-requirements
pdm deploy
```

## Pre-Prod deployment

TODO

## Prod deployment

TODO