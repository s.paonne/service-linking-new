import os
from pathlib import Path

from aws_cdk import (
    aws_events as events,
)

from constructs import Construct


class IntegrationTests(Construct):
    def __init__(self, scope: Construct, construct_id: str, eventbus: events.IEventBus):
        super().__init__(scope, construct_id)

        pass