import os
from pathlib import Path

from aws_cdk import (
    aws_lambda as lambda_,
    aws_lambda_python_alpha as python_lambda,
)
from constructs import Construct


class EmptyRequirementsException(BaseException):
    ...


class Layers(Construct):
    def __init__(self, scope: Construct, construct_id: str):
        super().__init__(scope, construct_id)

        if not os.path.exists(f"{Path(__file__).parents[1]}/python_layer/requirements.txt"):
            raise EmptyRequirementsException("requirements.txt in layers is either empty or doesn't exist")

        self.power = python_lambda.PythonLayerVersion(
            scope=self,
            id="Power",
            description="Powertools + Pydantic + Sentry + Simplicity-Tools = 9001",
            entry=f"{Path(__file__).parents[1]}/python_layer",
            compatible_runtimes=[lambda_.Runtime.PYTHON_3_9],
            compatible_architectures=[lambda_.Architecture.X86_64],
            bundling=python_lambda.BundlingOptions(
                environment={"PIP_EXTRA_INDEX_URL": os.getenv("PIP_EXTRA_INDEX_URL")},
            ),
        )
