from aws_cdk.aws_kms import Key

from constructs import Construct


class Keys(Construct):
    def __init__(self, scope: Construct, construct_id: str):
        super().__init__(scope, construct_id)

        self.dynamodb = Key.from_lookup(self, "KeyDynamoDB", alias_name="alias/sl-aws-dynamodb")
        self.lambda_ = Key.from_lookup(self, "KeyLambda", alias_name="alias/sl-aws-lambda")
        self.sns = Key.from_lookup(self, "KeySns", alias_name="alias/sl-aws-sns")
        self.sqs = Key.from_lookup(self, "KeySqs", alias_name="alias/sl-aws-sqs")
        self.s3 = Key.from_lookup(self, "KeyS3", alias_name="alias/sl-aws-s3")
        self.ssm = Key.from_lookup(self, "KeySsm", alias_name="alias/sl-aws-secrets-manager")
