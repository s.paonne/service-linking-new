from aws_cdk import (
    Stack,
)
from constructs import Construct


class AppStack(Stack):
    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # event_bus = EventBus.from_event_bus_name(self, "EventBus", event_bus_name="internal")
