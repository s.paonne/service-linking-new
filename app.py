#!/usr/bin/env python3
from typing import Any

from aws_cdk import (
    App,
    Aspects,
    IAspect,
    Tags,
    aws_lambda,
)
from caseconverter import pascalcase
from jsii import implements, member

from app.stack import (
    AppStack,
)
from env import (
    SENTRY_DSN,
    APP_NAME,
    DEPLOYMENT_ENV,
    SENTRY_ENVIRONMENT,
)


@implements(IAspect)
class LambdaEnvSetter:
    @member(jsii_name="visit")
    def visit(self, node: Any) -> None:
        if isinstance(node, aws_lambda.Function):
            node.add_environment("SENTRY_ENVIRONMENT", SENTRY_ENVIRONMENT)
            node.add_environment("SENTRY_DSN", SENTRY_DSN)


app = App()
Tags.of(app).add(key="team", value="Atlas")
Tags.of(app).add(key="service", value=APP_NAME)

stack = AppStack(scope=app, construct_id=pascalcase(APP_NAME), env=DEPLOYMENT_ENV)

Aspects.of(stack).add(LambdaEnvSetter())

app.synth()
