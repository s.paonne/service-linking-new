import os

from aws_cdk import (
    Environment,
)

APP_NAME = "{Name of the app}"
SENTRY_DSN = "{dsn of project}"

ATLAS_PROD_ID = "949855402213"
ATLAS_PRE_PROD_ID = "586465777960"

USER_ID = os.environ["CDK_DEFAULT_ACCOUNT"]
USER_REGION = os.environ["CDK_DEFAULT_REGION"]
DEPLOYMENT_ENV = Environment(account=USER_ID, region=USER_REGION)

PREPROD = USER_ID == ATLAS_PRE_PROD_ID
PROD = USER_ID == ATLAS_PROD_ID

if PROD:
    SENTRY_ENVIRONMENT = "production"
elif PREPROD:
    SENTRY_ENVIRONMENT = "pre-production"
else:
    SENTRY_ENVIRONMENT = f"developer:{os.getenv('USER')}"

print(f"{DEPLOYMENT_ENV=}::Environment: {SENTRY_ENVIRONMENT}")
